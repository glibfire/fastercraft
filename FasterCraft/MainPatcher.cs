﻿using System.Reflection;
using Harmony;

namespace FasterCraft     
{
    public class MainPatcher
    {
        public static void Patch()
        {
            var harmony = HarmonyInstance.Create("com.glibfire.graveyardkeeper.fastercraft.mod");   
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }

    }
}