﻿using Harmony;
using System;
using UnityEngine;
/*
 * Code to change crafting speed courtesy of Sroloc
 */

namespace FasterCraft
{
    [HarmonyPatch(typeof(CraftComponent))]
    [HarmonyPatch("DoAction")]
    class CraftComponent_Patcher
    {
        [HarmonyPrefix]
        static bool Prefix(CraftComponent __instance, ref WorldGameObject other_obj, ref float delta_time)
        {

            string line;
            string[] options;
            

            var path = @"./QMods/FasterCraft/config.txt";

            System.IO.StreamReader file = new System.IO.StreamReader(path);
            line = file.ReadLine();
            options = line.Split('=');

            if (__instance.current_craft.is_auto) delta_time *= (float)Convert.ToDouble(options[1]);
            if (!(__instance.current_craft.is_auto)) delta_time *= (float)Convert.ToDouble(options[1]);
            return true;
        }
    }
}
